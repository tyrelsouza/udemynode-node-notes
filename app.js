const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');
var titleOptions = {
  describe: 'Title of note',
  demand: true,
  alias: "t"
}
var bodyOptions = {
  describe: 'Body of note',
  demand: true,
  alias: 'b'
}

const argv = yargs
  .command('add', 'Add a new note', { title: titleOptions, body: bodyOptions })
  .command('list', 'List all notes')
  .command('read', 'Read a note', { title: titleOptions })
  .command('remove', 'Remove a note', { title: titleOptions })
  .help()
  .argv;
var command = argv._[0]
var noteTitle = argv.title;

switch (command){
  case 'add':
    var note = notes.addNote(noteTitle, argv.body);
    if (note){
      notes.logNote(note);
    } else {
      console.log(`No note added: ${noteTitle} exists already.`);
    }
    break;
  case 'list':
    var allNotes = notes.getAll();
    if (allNotes){
      console.log(`All Notes (${allNotes.length}):`);
      allNotes.forEach((note) =>{
        notes.logNote(note);
      });
    } else {
      console.log("No notes found.");
    }
    break;
  case 'read':
    var note = notes.getNote(noteTitle);
    if(note){
      notes.logNote(note);
    } else {
      console.log(`Can't find "${noteTitle}"`);
    }
    break;
  case 'remove':
    var noteRemoved = notes.removeNote(noteTitle);
    var message = noteRemoved ? `"${noteTitle}" was removed` : "No note removed";
    console.log(message);

    break;
  default:
      console.log("Command not found");
}
